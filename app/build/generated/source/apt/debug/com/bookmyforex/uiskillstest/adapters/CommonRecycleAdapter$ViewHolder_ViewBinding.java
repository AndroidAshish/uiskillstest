// Generated code from Butter Knife. Do not modify!
package com.bookmyforex.uiskillstest.adapters;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.bookmyforex.uiskillstest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CommonRecycleAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CommonRecycleAdapter.ViewHolder target;

  @UiThread
  public CommonRecycleAdapter$ViewHolder_ViewBinding(CommonRecycleAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.tv_status = Utils.findRequiredViewAsType(source, R.id.tv_status, "field 'tv_status'", TextView.class);
    target.tv_gratification = Utils.findRequiredViewAsType(source, R.id.tv_gratification, "field 'tv_gratification'", TextView.class);
    target.tv_date = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tv_date'", TextView.class);
    target.tv_title = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tv_title'", TextView.class);
    target.iv_item = Utils.findRequiredViewAsType(source, R.id.iv_item, "field 'iv_item'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CommonRecycleAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tv_status = null;
    target.tv_gratification = null;
    target.tv_date = null;
    target.tv_title = null;
    target.iv_item = null;
  }
}
