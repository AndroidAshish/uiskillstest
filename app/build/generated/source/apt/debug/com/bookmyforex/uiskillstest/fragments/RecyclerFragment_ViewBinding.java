// Generated code from Butter Knife. Do not modify!
package com.bookmyforex.uiskillstest.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.bookmyforex.uiskillstest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RecyclerFragment_ViewBinding implements Unbinder {
  private RecyclerFragment target;

  private View view2131230756;

  @UiThread
  public RecyclerFragment_ViewBinding(final RecyclerFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.sp_from = Utils.findRequiredViewAsType(source, R.id.sp_from, "field 'sp_from'", Spinner.class);
    target.sp_to = Utils.findRequiredViewAsType(source, R.id.sp_to, "field 'sp_to'", Spinner.class);
    target.tv_ToDate = Utils.findRequiredViewAsType(source, R.id.tv_ToDate, "field 'tv_ToDate'", TextView.class);
    target.tv_FromDate = Utils.findRequiredViewAsType(source, R.id.tv_FromDate, "field 'tv_FromDate'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_vew, "method 'onClick'");
    view2131230756 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RecyclerFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.sp_from = null;
    target.sp_to = null;
    target.tv_ToDate = null;
    target.tv_FromDate = null;

    view2131230756.setOnClickListener(null);
    view2131230756 = null;
  }
}
