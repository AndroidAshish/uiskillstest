// Generated code from Butter Knife. Do not modify!
package com.bookmyforex.uiskillstest.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.bookmyforex.uiskillstest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PostsFragment_ViewBinding implements Unbinder {
  private PostsFragment target;

  @UiThread
  public PostsFragment_ViewBinding(PostsFragment target, View source) {
    this.target = target;

    target.tv_heading = Utils.findRequiredViewAsType(source, R.id.tv_heading, "field 'tv_heading'", TextView.class);
    target.tv_money = Utils.findRequiredViewAsType(source, R.id.tv_money, "field 'tv_money'", TextView.class);
    target.tv_heading2 = Utils.findRequiredViewAsType(source, R.id.tv_heading2, "field 'tv_heading2'", TextView.class);
    target.tv_money2 = Utils.findRequiredViewAsType(source, R.id.tv_money2, "field 'tv_money2'", TextView.class);
    target.seekBar = Utils.findRequiredViewAsType(source, R.id.seekBar, "field 'seekBar'", SeekBar.class);
    target.seekBar2 = Utils.findRequiredViewAsType(source, R.id.seekBar2, "field 'seekBar2'", SeekBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PostsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tv_heading = null;
    target.tv_money = null;
    target.tv_heading2 = null;
    target.tv_money2 = null;
    target.seekBar = null;
    target.seekBar2 = null;
  }
}
