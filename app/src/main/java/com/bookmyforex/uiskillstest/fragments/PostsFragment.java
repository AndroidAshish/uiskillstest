package com.bookmyforex.uiskillstest.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bookmyforex.uiskillstest.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PostsFragment extends Fragment {

    @BindView(R.id.tv_heading)
    TextView tv_heading;
    @BindView(R.id.tv_money)
    TextView tv_money;
    @BindView(R.id.tv_heading2)
    TextView tv_heading2;
    @BindView(R.id.tv_money2)
    TextView tv_money2;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.seekBar2)
    SeekBar seekBar2;

    public static Fragment newInstance(Context context) {
        PostsFragment f = new PostsFragment();
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.content_main, null);
        setUpView(root);
        return root;
    }

    void setUpView(ViewGroup root){
        ButterKnife.bind(this, root);
        setUPList();
        setUPListener();
    }

    void setUPListener(){

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                // TODO Auto-generated method stub

                tv_money.setText("₹​ "+NumberFormat.getNumberInstance(Locale.US).format(progress));

            }
        });

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                // TODO Auto-generated method stub

                tv_money2.setText("₹​ "+NumberFormat.getNumberInstance(Locale.US).format(progress));

            }
        });

    }

    void setUPList(){

        String s= tv_heading.getText().toString();
        SpannableString ss1=  new SpannableString(s);
        ss1.setSpan(new RelativeSizeSpan(0.99f), 51,s.length(), 0); // set size
        ss1.setSpan(new ForegroundColorSpan(Color.parseColor("#5C5C5C")), 51, s.length(), 0);// set color
        tv_heading.setText(ss1);

        String s1= tv_heading2.getText().toString();
        SpannableString ss2=  new SpannableString(s1);
        ss2.setSpan(new RelativeSizeSpan(0.99f), 46,s1.length(), 0); // set size
        ss2.setSpan(new ForegroundColorSpan(Color.parseColor("#5C5C5C")), 46, s1.length(), 0);// set color
        tv_heading2.setText(ss2);
    }

    private List<String> createItemList() {
        List<String> itemList = new ArrayList<>();
        for(int i=0;i<30;i++) {
            itemList.add("Item "+i);
        }
        return itemList;
    }

}
