package com.bookmyforex.uiskillstest.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bookmyforex.uiskillstest.R;
import com.bookmyforex.uiskillstest.adapters.CommonRecycleAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecyclerFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sp_from)
    Spinner sp_from;
    @BindView(R.id.sp_to)
    Spinner sp_to;
    @BindView(R.id.tv_ToDate)
    TextView tv_ToDate;
    @BindView(R.id.tv_FromDate)
    TextView tv_FromDate;
    String dateFromAlias;
    String dateToAlias;
    final Calendar myCalendar = Calendar.getInstance();
    private SimpleDateFormat dateFormatter;
    private  SimpleDateFormat sdf;

    public static Fragment newInstance(Context context) {
        PostsFragment f = new PostsFragment();
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.content_main_recycler, null);
        setUpView(root);
        return root;
    }

    void setUpView(ViewGroup root) {
        ButterKnife.bind(this, root);
        dateFormatter = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
        sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setUPListener();
    }

    void setUPList() {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        CommonRecycleAdapter adapter = new CommonRecycleAdapter(createItemList());
        recyclerView.setAdapter(adapter);
    }

    void setUPListener() {

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();
            }

        };

        tv_FromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        tv_ToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel(){
        tv_ToDate.setText(dateFormatter.format(myCalendar.getTime()));
        dateToAlias = sdf.format(myCalendar.getTime());
    }

    private void updateLabel1(){
        tv_FromDate.setText(dateFormatter.format(myCalendar.getTime()));
        dateFromAlias = sdf.format(myCalendar.getTime());

    }

    @OnClick(R.id.btn_vew)
    public void onClick(){

        if(!tv_FromDate.getText().toString().equalsIgnoreCase("")&&
                !tv_ToDate.getText().toString().equalsIgnoreCase("")){
            try {
                Date date1 = sdf.parse(dateFromAlias);
                Date date2 = sdf.parse(dateToAlias);
                if(date1.after(date2)){
                    Toast.makeText(getActivity(), "Enter valid dates!", Toast.LENGTH_SHORT).show();
                    if(recyclerView.getVisibility()== View.VISIBLE){
                        recyclerView.setVisibility(View.GONE);
                    }
                }else{
                    setUPList();
                }
            }catch (ParseException pe){
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getActivity(), "Enter valid dates!", Toast.LENGTH_SHORT).show();
        }

    }

    private List<HashMap<String, String>> createItemList() {
        List<HashMap<String, String>> itemList = new ArrayList<>();
        String[] titles = {"Harvery Norman Buket Tinkel", "Hudson Bar And Books", "Sleep Nice Motel"
                , "Adidas, Pacific Square"};
        String[] dates = {"19 Jul 18, 11:36", "22 Jul 18, 22:41", "23 Jul 18, 22:41"
                , "19 Jul 18, 11:36"};
        String[] status = {"+$4.87", "-$9.28", "-$12.76"
                , "+$4.87"};
        String[] gratification = {"+ ₹340.08", "- ₹648.12", "-₹890.75"
                , "+₹340.08"};

        HashMap<String, String> hmap;
        for (int i = 0; i < 4; i++) {
            hmap = new HashMap<>();
            hmap.put("Title", titles[i]);
            hmap.put("Date", dates[i]);
            hmap.put("Status", status[i]);
            hmap.put("Gratification", gratification[i]);
            itemList.add(hmap);
        }
        return itemList;
    }
}
