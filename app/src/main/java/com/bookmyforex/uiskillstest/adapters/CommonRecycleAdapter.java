package com.bookmyforex.uiskillstest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bookmyforex.uiskillstest.R;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommonRecycleAdapter extends RecyclerView.Adapter<CommonRecycleAdapter.ViewHolder> {

    private List<HashMap<String, String >> itemsData;
    int[] myImageList = new int[]{R.drawable.star, R.drawable.bills, R.drawable.cart, R.drawable.bag};

    public CommonRecycleAdapter(List<HashMap<String, String>> itemsData) {
        this.itemsData = itemsData;
    }

    @Override
    public CommonRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_common_items, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.tv_date.setText(itemsData.get(position).get("Date"));
        viewHolder.tv_title.setText(itemsData.get(position).get("Title"));
        viewHolder.tv_status.setText(itemsData.get(position).get("Status"));
        viewHolder.tv_gratification.setText(itemsData.get(position).get("Gratification"));
        viewHolder.iv_item.setImageResource(myImageList[position]);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_status)
        TextView tv_status;
        @BindView(R.id.tv_gratification)
        TextView tv_gratification;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.iv_item)
        ImageView iv_item;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}