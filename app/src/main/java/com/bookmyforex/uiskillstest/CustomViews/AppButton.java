package com.bookmyforex.uiskillstest.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.bookmyforex.uiskillstest.R;

public class AppButton extends AppCompatButton {

    public AppButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AppButton(Context context) {
        super(context);
        init(context, null);
    }

    public void init(Context context, AttributeSet attrs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setBackground(ContextCompat.getDrawable(context, R.drawable.button_ripple_bg));
            setTransformationMethod(null);
            setElevation(0);
        } else {
            setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.rounded_button));
        }
        setTextColor(ContextCompat.getColor(context, R.color.color_white));

        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.dimen_14dp));
    }
}